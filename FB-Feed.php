<?php
/*
Plugin Name: KathrynFreemanWeb-FbNewsFeed
Plugin URI: 
Description: A feed of the Facebook posts on Kathryn Freeman (author) Facebook page
Version: 0.1
Author: James Harding
Author URI: http://jamesharding.me
License: GPL
*/

	add_action('admin_menu', 'fbFeed_admin_add_page');
		function fbFeed_admin_add_page() {
		add_options_page('Facebook Feed Options', 'Facebook Feed Options', 'manage_options', 'fbFeed', 'fbFeed_options_page');
	}

	function fbFeed_options_page() {
		echo "<div class='wrap'>" . 
		"<h2>Facebook Feed Options</h2>" .
		"<form action='options.php' method='post'>";
		settings_fields('fbFeed_options');
		do_settings_sections('fbFeed');
		
		submit_button();
		echo "</form></div>";
 	}

	add_action('admin_init', 'fbFeed_admin_init');
	function fbFeed_admin_init(){
		register_setting( 'fbFeed_options', 'fbFeed_pageID');
		register_setting( 'fbFeed_options', 'fbFeed_aToken');
		add_settings_section('fbFeed_main', 'Feed Settings', 'fbFeed_section_text', 'fbFeed');
		add_settings_field('fbFeed_pageID', 'Facebook Page ID', 'fbFeed_setting_pageID', 'fbFeed', 'fbFeed_main');
		add_settings_field('fbFeed_aToken', 'Access Token', 'fbFeed_setting_aToken', 'fbFeed', 'fbFeed_main');
	}

	function fbFeed_section_text() {
		echo '<p>Main description of this section here.</p>';
	}

	function fbFeed_setting_pageID() {
		$options = get_option('fbFeed_pageID');
		echo "<input id='fbFeed_text_pageID' name='fbFeed_pageID' type='text' value='" . get_option("fbFeed_pageID") . "' />";
	}

	function fbFeed_setting_aToken() {
		$options = get_option('fbFeed_aToken');
		echo "<input id='fbFeed_text_aToken' name='fbFeed_aToken' type='text' value='" . get_option("fbFeed_aToken") . "' />";
	}

	function makeFbNewsFeed() {
		//add_option(fbPageID, )
		echo "<link rel='stylesheet' type='text/css' href='" . plugins_url( 'style.css', __FILE__ ) . "'>"
		. "<script type='text/javascript' src='" . plugins_url( 'jquery.ellipsis.min.js', __FILE__ ) . "'></script>" 
		. "<script type='text/javascript' src='" . plugins_url( 'feed.min.js', __FILE__ ) . "'></script>"
		. "<div id='feed'><h1 id='fbNewsTitle'>Latest News</h1><div id='loading'><img src='" . plugins_url( 'ajax-loader.gif', __FILE__ ) . "' alt='loading'></div></div>"	
		. "<input id='pageIDVal' type='hidden' value='" .  get_option('fbFeed_pageID') . "' />"
		. "<input id='aTokenVal' type='hidden' value='" .  get_option('fbFeed_aToken') . "' />";
	}

	add_shortcode("makefeed", "makefeed_func");
	function makefeed_func() {
		return makeFbNewsFeed();
	}
?>
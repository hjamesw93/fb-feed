var $ = jQuery.noConflict();
$(document).ready(function() {
	
	var pageID =  $("#pageIDVal").val();
	var aToken = $("#aTokenVal").val();
	
	$.ajax({
	  dataType: "json",
	  url: "https://graph.facebook.com/" + pageID + "/feed?access_token=" + aToken,
	  success: function(data){
	  	console.log( 'success' );
	    console.dir( data );
	    
	    $("#loading").remove();

	    obj = data.data;
	    var i = 0;
	    Object.keys(obj).forEach(function(key){
			if(obj[key].from.id == pageID && obj[key].message != null && i < 3) {
				var id = obj[key].id;
	    		var idparts = id.split('_');
	    		var id1 = idparts[1];
	    		var id2 = idparts[0];
	    		var likes = 0;
	    		var comments = 0;
	    		
	    		//If likes exist for post:
	    		if(obj[key].likes != null) { 
	    			likes = obj[key].likes.count 
	    		} 
	    		else { 
	    			likes = 0
	    		}

	    		//If comments exist for post:
	    		if(obj[key].comments != null) {
	    			comments = (obj[key].comments.data).length;
	    		}
	    		else {
	    			comments = 0;
	    		}

	    		//Split title up into seperate strings
	    		var postTitle = (obj[key].message).split(" ");
	    		var newTitle = "";
	    		var a = 0;

	    		while(a < postTitle.length) {
	    			titlePart = postTitle[a];
	    			
	    			//Now split up each character
	    			chars = titlePart.split(""); 
	    			
	    			var b =1;
	    			if(chars[0] != null) {
	    				titlePart = (chars[0]).toUpperCase();
	    			}
	    			
	    			while(b < chars.length) {
	    				titlePart += chars[b];
	    				b++;
	    			}
	    			
	    			newTitle += " " + titlePart;
	    			a++;
	    		}

	    		$('#feed').append(
	    			"<div class='post' id='" + id1 + "'>" + 
	    				"<div class='postTitle'>" + 
	    					newTitle + 
	    				"</div>" +
	    				"<div class='postContent'>" +
	    					obj[key].message +
	    				"</div>" + 
	    				"<div class='postFooter'>" + 
	    					likes + " likes " + comments + " comments " +  
	    					"<a class='fbLink' target='blank' href='https://www.facebook.com/permalink.php?story_fbid=" + id1 + "&id=" + id2 + "'>Continue Reading...</a>" +
	    				"</div>" +
	    				"<div id='rule" + i + "'></div>" +
	    			"</div>");
	    		i++;
    		}
    		//Limit output with jquery ellipsis
			$(".postTitle").ellipsis({
				row: 1
			});
			$(".postContent").ellipsis({
				row: 4
			});
	    });
	  }	    
	});
});
